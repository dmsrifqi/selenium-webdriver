package com.selenium.app.lazismu;

public class ThreadClass implements Runnable {

	private String[] data;
	
	//String date, modul, appName, url, nama fitur, log
	public void startThread(String[] data) {
		this.data = data;
	}
	
	@Override
	public void run() {
		try {
			System.out.println("Run thread >>>>");
			String[] param = {
				this.data[0],
				this.data[5],
				this.data[3]
			};
			new SendToExcelLazismu(param);
			new SendToEmailLazismu(this.data);
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
