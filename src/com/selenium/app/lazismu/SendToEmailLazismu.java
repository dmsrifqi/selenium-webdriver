package com.selenium.app.lazismu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendToEmailLazismu {
	
	private Properties prop;
	
	public SendToEmailLazismu(String[] data) {
		System.out.println("Send to email >>>>");
		sendReport(data);
	}

	private void sendReport(String[] data) {
		
		File file = new File("setting\\setting.properties");
		try {
			FileInputStream fis = new FileInputStream(file);
			Properties prop = new Properties();
			this.prop = prop; 
			this.prop.load(fis);
			
			String path = this.prop.getProperty("automation.app.config.dir");
			File file2 = new File(path+"Java\\\\lazismu-oase.properties");
			
			FileInputStream fis2 = new FileInputStream(file2);
			this.prop = new Properties();
			this.prop.load(fis2);
			
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String getEmail = this.prop.getProperty("from.email");
		String getPass = this.prop.getProperty("from.pass");
		
		final String email = getEmail;
		final String password = getPass;
		
		Properties prop = new Properties();
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.starttls.enable", "true");
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.port", "587");
		
		Session session = Session.getInstance(prop, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(email, password);
			}
		});
		
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(email));
			
			String getEmailReceive = this.prop.getProperty("to.email.receive");
			String[] splitEmail = getEmailReceive.replaceAll(" ", "").split(";");
			for (int i = 0; i < splitEmail.length; i++) {
				message.setRecipients(Message.RecipientType.TO,
						InternetAddress.parse(splitEmail[i]));
			}
			
			String ccEmail = this.prop.getProperty("to.email.cc");
			String[] arrCCEmail = ccEmail.replaceAll(" ", "").split(";");
			
			for (int i = 0; i < arrCCEmail.length; i++) {
				message.addRecipient(Message.RecipientType.CC, 
						new InternetAddress(arrCCEmail[i]));
			}
		
			message.setSubject("[AUTOMATION-FUNCTIONAL-TEST] " + data[2] + " | " + data[1]);
			message.setText(
					"Nama projek: " + data[2] + "\n" + 
					"URL projek: " + data[3]+"\n"+ 
					"Tanggal runtime: " + data[0] + "\n"+
					"====================================="+ "\n" +
					"Fitur: " + data[4] + "\n" +
					"Log: " + data[5] + "\n"
			);
			
			Transport.send(message, message.getAllRecipients());
			System.out.println("Email has been send ! >>>>");
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
}
