package com.selenium.app.lazismu;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SendToExcelLazismu {
	
	private Properties prop = new Properties();
	private FileInputStream fis;
	private Logger logger = Logger.getLogger("SendToExcel");
	private static XSSFSheet sheet;
	private static XSSFWorkbook workbook;
	private String finalTimeNow;
	
	public SendToExcelLazismu(String[] param) {
		
		String timeNow = param[0];
		String log = param[1];
		String getCurrentUrl = param[2];
		
		finalTimeNow = timeNow;
		
		File file = new File("D:\\Java\\lazismu-oase.properties");
		
		workbook = new XSSFWorkbook();
     	sheet = workbook.createSheet(finalTimeNow);
		
		try {
			fis = new FileInputStream(file);
			prop.load(fis);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String[] data = {				
				log,
				getCurrentUrl
		};
		
		writeError(data);
	}
	
	public void writeError(String[] data) {
		
		String log = data[0];
		String getCurrentUrl = data[1];
		
		Object[][] datatypes = {
                {finalTimeNow, log, getCurrentUrl}
        };
		
		headerCell();
		createNewExcel(datatypes);
		
	}

	private void createNewExcel(Object[][] datatypes) {
	
		logger.info("Creating new excel >>>>");
		int rowNum = 1;
		for (Object[] datatype : datatypes) {
            Row row = sheet.createRow(rowNum++);
            sheet.setColumnWidth(rowNum++, 7500);
            int colNum = 0;
            for (Object field : datatype) {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String) {
                	String getVal = cell.getStringCellValue();
                	if( getVal.length() == 0 ) {
                		cell.setCellValue((String) field);
                	}
                } else if (field instanceof Integer) {
                	Double getVal = cell.getNumericCellValue();
                	if(getVal == 0) {
                		cell.setCellValue((Integer) field);
                	}
                }
            }
	    }
	        
	    try {
	    	String getFileName =  prop.getProperty("log.error.excel.file.path");
	    	logger.info("getFileName: " + getFileName);
	    	String finalGetFileName = getFileName + "lazismu_" + finalTimeNow + ".xlsx";
	    	logger.info("FileName: " + finalGetFileName);
	    	
	        FileOutputStream outputStream = new FileOutputStream(finalGetFileName);
	        workbook.write(outputStream);
	        workbook.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
	       
	    logger.info("Excel file has been created >>>>");
		
	}

	private void headerCell() {
		
		logger.info("Creating header cell >>>>");
		String[] headers = new String[] { "Tanggal", "Log", "URL" };
        for (int i=0; i<headers.length; i++) {
           Row r = sheet.createRow(0);
           for(int j=0; j<headers.length; j++ ) {
        	   r.createCell(j).setCellValue(headers[j]);   
           }
        }
        
        logger.info("Header cell has been created >>>>");
	}

}
