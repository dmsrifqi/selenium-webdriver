package com.selenium.app.lazismu;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

public class Lazismu {
	
	private WebDriver objDriver;
	private Logger logger = Logger.getLogger("Lazismu");
	private Properties p, pathLoc;
	private String path, app;
	private FileInputStream fis, fis2, fisSetting;
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
	private Date date = new Date();
	
	public Lazismu(String app) throws Exception {
		logger.info("Lazismu automation has been started !");
		this.app = app;
		PropertyConfigurator.configure("D:\\Java\\log.properties");		//Load konfigurasi library buat log
		loadProperties();												//Calling function
		startAutomation();
	}
	
	public Boolean CheckIfElementIsVisible()
	{	
		Boolean isVisible = objDriver.findElement(By.xpath("//*[@id='tve_editor']/div[2]/div[2]/div/div")).isDisplayed();
	    if (isVisible) {
			return true;
		} else {
			return false;
		}
	}

	private void loadProperties() {
		try {
			
			File file = new File("setting\\setting.properties");
			fis = new FileInputStream(file);
			Properties p = new Properties();
			p.load(fis);
			
			File fileSetting = new File("setting\\setting.properties");
			fisSetting = new FileInputStream(fileSetting);
			
			pathLoc = new Properties();
			pathLoc.load(fisSetting);
			path = pathLoc.getProperty("automation.app.config.dir");	//Ambil path partisi komputer, contoh: D: Local Disk, E: Local Disk untuk diset ke object path.
			logger.info("Local Disk: "+path);
			
			File file2 = new File(path+"Java\\lazismu-oase.properties");
			fis2 = new FileInputStream(file2);
			
			Properties prop = new Properties();
			this.p = prop;
			this.p.load(fis2);
			
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("There is error message: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("There is error message: " + e.getMessage());
		} finally {
			try {
				fis2.close();
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("There is error message: " + e.getMessage());
			}
		}
		
	}
	
	//Open browser
	public void chromeBrowser() {
		System.setProperty("webdriver.chrome.driver", path + "Java\\ChromeDriver\\chromedriver.exe");
		objDriver = new ChromeDriver();
		objDriver.get("http://lazismu.zakat.dev.codigo.id/welcome");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("moz:firefoxOptions", true);
	}
	
	//Open browser
	public void firefoxBrowser() {
		System.setProperty("webdriver.gecko.driver", path + "Java\\Gecko\\geckodriver-v0.18.0-win64\\geckodriver.exe");
		objDriver = new FirefoxDriver();
		objDriver.get("http://lazismu.zakat.dev.codigo.id/welcome");
	}

	
	//Automation test akan dimulai dari sini, karena function ini yang akan memanggil browser pertama kali dan akan melakukan step selanjutnya sekali berurut.
	public void startAutomation() throws Exception {
		logger.info("INFO : Open browser ! >>>>");
		
		File file = new File("setting\\setting.properties");
		FileInputStream fis = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fis);
		
		//Cek browser yang dipilih
		if( prop.getProperty("automation.app.browser").equalsIgnoreCase("firefox") ) {
			firefoxBrowser();
		} else if( prop.getProperty("automation.app.browser").equalsIgnoreCase("chrome") ) {
			chromeBrowser();
		} else {
			logger.info("Browser not found !");
			System.exit(0); //Kalo browser yang dipilih tidak sesuai maka aplikasi akan exit.
		}
		
		logger.info("http://lazismu.zakat.dev.codigo.id/welcome has been opened ! >>>> ");
		login();
		createNewOase(); //create oase dimulai !
	}
	
	@SuppressWarnings("static-access")
	private void timeout(int param) {
		try {
			Thread t = new Thread();
			t.sleep(param);
		} catch (InterruptedException e) {
			e.printStackTrace();
			logger.error("There is error message: " + e.getMessage());
		}
	}
	
	private void uploadThumbnail() throws Exception {
		logger.info("Upload thumbnail >>>> ");
		Robot robot = new Robot();
		
		//Klik button add thumbnail, pada step ini akan muncul pop up explorer windows untuk memilih gambar
		objDriver.findElement(By.id("gambar1tambah")).click();
		String thumbnail = p.getProperty("oase.thumbnail.location");
		StringSelection stringSel = new StringSelection(thumbnail);
		
		//copy lokasi gambar
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSel, null);
		
		//paste lokasi gambar
		robot.setAutoDelay(1000);
		robot.keyPress(KeyEvent.VK_CONTROL);
		robot.keyPress(KeyEvent.VK_V);
		
		robot.keyRelease(KeyEvent.VK_CONTROL);
		robot.keyRelease(KeyEvent.VK_V);
		
		//pencet tombol enter untuk select gambar yang udah dipilih
		robot.setAutoDelay(1000);
		robot.keyPress(KeyEvent.VK_ENTER);
		robot.keyRelease(KeyEvent.VK_ENTER);
		
		WebElement thumbs = objDriver.findElement(By.id("setthumbs_img_path_tambah"));
		
		JavascriptExecutor js = (JavascriptExecutor) objDriver;
		js.executeScript("arguments[0].click();", thumbs);
		
		timeout(2000);
		
	}
	
	private void qurbanProgram(String getJenisPro) {
		//Klik dropdown
		WebElement webElement = objDriver.findElement(By.id("jenis_pro"));
		Select selectDropdown = new Select(webElement);
		selectDropdown.selectByVisibleText(getJenisPro);
		
		objDriver.findElement(By.id("kambing")).click();
		
		//Klik dropdown untuk menentukan jumlah hewan yang mau diprogram.
		WebElement webElement2 = objDriver.findElement(By.id("max_unit_kambing"));
		Select selectDropdown2 = new Select(webElement2);
		selectDropdown2.selectByVisibleText("2");
		
		//Tentukan title untuk judul oase.
		WebElement getElementNamaProgram = objDriver.findElement(By.name("title"));
		String namePro = p.getProperty("oase.program.qurban.name");
		getElementNamaProgram.sendKeys(namePro);
		logger.info("Set nama program >>>> " + namePro);
		
		//Input harga
		getElementNamaProgram.sendKeys(Keys.TAB);
		WebElement getElementHargaSatuEkor = objDriver.switchTo().activeElement();
		String getHarga = p.getProperty("oase.program.qurban.harga");
		int val = Integer.valueOf(getHarga);
		String finalVal = ""+val;
		getElementHargaSatuEkor.sendKeys(finalVal);
		logger.info("Harga yang dimasukan: " + finalVal);

		//pilih tanggal mulai
		WebElement getElementDateStart = objDriver.findElement(By.xpath("//*[@id=\"date_start_\"]"));
		getElementDateStart.clear();
		String getDateStart = p.getProperty("oase.program.qurban.dateStart");
		getElementDateStart.sendKeys(getDateStart);
		logger.info("Set tanggal mulai program >>>> " + getDateStart);
		
		//pilih tanggal selesai
		WebElement getElementDateEnd = objDriver.findElement(By.id("date_end_"));
		getElementDateEnd.clear();
		String getDateEnd = p.getProperty("oase.program.qurban.dateEnd");
		getElementDateEnd.sendKeys(getDateEnd);
		logger.info("Set tanggal berhenti program >>>> " + getDateEnd);

		//set lokasi
		WebElement getElementAlokasiProgram = 
		objDriver.findElement(By.xpath("/html/body/section/div[1]/section/section/div[3]/div/section/div/form/div[10]/div/input[@name='place_name']"));
		getElementAlokasiProgram.clear();
		String getLoc = p.getProperty("oase.program.qurban.place");
		getElementAlokasiProgram.sendKeys(getLoc);
		logger.info("Set alokasi program >>>> " + getLoc);
		
		//set deskripsi
		WebElement getElementDeskripsi = objDriver.findElement(By.xpath("//*[@id=\"description\"]"));
		getElementDeskripsi.clear();
		String getDesc = p.getProperty("oase.program.qurban.description");
		getElementDeskripsi.sendKeys(getDesc);
		logger.info("Set description >>>> " + getDesc);
		
		//submit
		objDriver.findElement(By.id("sabmit")).submit();
		logger.info("Program telah disubmit >>>>");
		
	}
	
	private void regulerProgram() {
		//Tentukan title untuk judul oase.
		String namePro = p.getProperty("oase.program.regular.name");
		WebElement getElementNamaProgram = objDriver.findElement(By.name("title"));
		getElementNamaProgram.sendKeys(namePro);	
		logger.info("Set nama program >>>> " + namePro);
		
		//pilih tanggal mulai
		String getDateStart = p.getProperty("oase.program.regular.dateStart");
		WebElement getElementDateStart = objDriver.findElement(By.xpath("//*[@id=\"date_start_\"]"));
		getElementDateStart.clear();
		getElementDateStart.sendKeys(getDateStart);
		logger.info("Set tanggal mulai program >>>> " + getDateStart);
		
		//pilih tanggal selesai
		String getDateEnd = p.getProperty("oase.program.regular.dateEnd");
		WebElement getElementDateEnd = objDriver.findElement(By.id("date_end_"));
		getElementDateEnd.clear();
		getElementDateEnd.sendKeys(getDateEnd);
		logger.info("Set tanggal berhenti program >>>> " + getDateEnd);
		
		//set lokasi
		String getLoc = p.getProperty("oase.program.regular.place");
		WebElement getElementAlokasiProgram = 
		objDriver.findElement(By.xpath("/html/body/section/div[1]/section/section/div[3]/div/section/div/form/div[10]/div/input[@name='place_name']"));
		getElementAlokasiProgram.clear();
		getElementAlokasiProgram.sendKeys(getLoc);
		logger.info("Set alokasi program >>>> " + getLoc);
		
		//Input harga
		String getHarga = p.getProperty("oase.program.regular.harga");
		getElementAlokasiProgram.sendKeys(Keys.TAB);
		WebElement getElementHargaSatuEkor = objDriver.switchTo().activeElement();
		int val = Integer.valueOf(getHarga);
		String finalVal = ""+val;
		getElementHargaSatuEkor.sendKeys(finalVal);
		logger.info("Harga yang dimasukan: " + finalVal);
		
		WebElement getTampilkanPadaAplikasi = objDriver
		.findElement(By.xpath("/html/body/section/div[1]/section/section/div[3]/div/section/div/form/div[11]/div/div[2]/div/div"));
		getTampilkanPadaAplikasi.click();

		//set deskripsi
		String getDesc = p.getProperty("oase.program.qurban.description");
		WebElement getElementDeskripsi = objDriver.findElement(By.xpath("//*[@id=\"description\"]"));
		getElementDeskripsi.clear();
		getElementDeskripsi.sendKeys(getDesc);
		logger.info("Set description >>>> " + getDesc);
		
		//Submit
		objDriver.findElement(By.id("sabmit")).submit();
		logger.info("Program telah disubmit >>>>");
		
		objDriver.close();
	}
	
	private void addPrograms() throws Exception {
		logger.info("Add programs >>>> ");
		timeout(5000);
		objDriver.findElement(By.id("addprograms")).click();
		
		uploadThumbnail();
		
		String getJenisPro = p.getProperty("oase.program.type");
		
		//Pilih jenis program
		if( getJenisPro.equalsIgnoreCase("qurban")) {
			qurbanProgram(getJenisPro);
		} else if( getJenisPro.equalsIgnoreCase("program regular") ) {
			regulerProgram();
		}
		
	}

	public void createNewOase() throws Exception {
		logger.info("Create Oase >>>> ");
		timeout(3000);
		objDriver.findElement(By.id("oase")).click();
		objDriver.findElement(By.id("daftar_program")).click();
		addPrograms();  //add programs ( kondisi ini sudah masuk module oase )
	}

	public void login() {
		logger.info("Login into Lazismu >>>> ");
		String username = p.getProperty("login.username");
		String password = p.getProperty("login.password");
		objDriver.findElement(By.id("mail")).sendKeys(username);
		objDriver.findElement(By.name("passwd")).sendKeys(password);
		objDriver.findElement(By.id("realsub")).click();
		
		if(objDriver.findElements(By.xpath("/html/body/div/form[1]/div/p")).size() != 0) {
			
			List<String> getMultipleElement = new ArrayList<>();
			
			//Get element error jika ada error
			for(int i=0; i<3; i++) {
				List<WebElement> allElement = objDriver.findElements(By.xpath("/html/body/div/form[1]/div/p["+i+"]"));
				for(WebElement w: allElement) {
					getMultipleElement.add(w.getText());
				}
			}
			
			if( getMultipleElement.size() == 2) {
				WebElement getElementErrorUser = objDriver.findElement(By.xpath("/html/body/div/form[1]/div/p[1]"));
				WebElement getElementErrorPass = objDriver.findElement(By.xpath("/html/body/div/form[1]/div/p[2]"));
				
				String getElementErrorUserAndPass = getElementErrorUser.getText()+" "+getElementErrorPass.getText();
				logger.error("There's error message: " + getElementErrorUserAndPass);
				String[] data = {dateFormat.format(date), "Login", this.app, objDriver.getCurrentUrl(), "Login", getElementErrorUserAndPass};
				
				System.out.println(dateFormat.format(date)); //2016/11/16 12:08:43
				
				ThreadClass out = new ThreadClass();
				out.startThread(data);
				out.run();
				
				System.exit(0);
				
			} else {
				WebElement getElementError = objDriver.findElement(By.xpath("/html/body/div/form[1]/div/p"));
				logger.error("There's error message: " + getElementError.getText());
				String[] data = {dateFormat.format(date), "Login", this.app, objDriver.getCurrentUrl(), "Login", getElementError.getText()};
				ThreadClass out3 = new ThreadClass();
				out3.startThread(data);
				out3.run();
				System.exit(0);
			}
		}
	}
}
