package com.selenium.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;

import com.selenium.app.lazismu.Lazismu;
import com.selenium.app.netportal.Netportal;
import com.selenium.app.spotlight.Spotlight;

public class Main {
	
	private static Logger logger = Logger.getLogger("Main");
	private static FileInputStream fis;
	
	@Before
	public static void run() {
		
		logger.info("Selenium webdriver starting >>>>");
		logger.info("Checking file setting.properties >>>>");
		try {
			File file = new File("D:\\Java\\setting.properties");	//load file setting.properties
			fis = new FileInputStream(file);
			
			Properties p = new Properties();
			p.load(fis);
			String app = p.getProperty("automation.app.test.name"); //ambil parameter yang ada di dalam setting.properties
			
			if( app.equalsIgnoreCase("lazismu")) {
				new Lazismu(app);		//Jalanin construct dari class Lazismu (Detail lihat di file Lazismu.java)
				logger.info("Kill geckodriver.exe");
				Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
			} else if( app.equalsIgnoreCase("netportal")) {
				new Netportal();
				logger.info("Kill geckodriver.exe");
				Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
			} else if( app.equalsIgnoreCase("spotlight")) {
				new Spotlight(p);
				logger.info("Kill geckodriver.exe");
				Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("There is error message: " + e.getMessage());
			try {
				logger.info("Kill geckodriver.exe");
				Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("There is error message: " + e.getMessage());
			try {
				logger.info("Kill geckodriver.exe");
				Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("There is error message: " + e.getMessage());
			try {
				logger.info("Kill geckodriver.exe");
				Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("There is error message: " + e.getMessage());
				try {
					logger.info("Kill geckodriver.exe");
					Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	public static void main(String[] args) throws Exception {
		PropertyConfigurator.configure("D:\\Java\\log.properties");
		run();
	}

}
