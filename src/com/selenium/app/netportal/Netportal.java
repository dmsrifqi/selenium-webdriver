package com.selenium.app.netportal;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

import com.selenium.app.netportal.SendToExcelNetportal;

public class Netportal {
	
	public WebDriver objDriver;
	private Logger logger = Logger.getLogger("Netportal");
	private Properties p;
	private Properties pathLoc;
	private String path;
	private FileInputStream fis;
	private FileInputStream fis2;
	private FileInputStream fisSetting;
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
	private Date date = new Date();
	
	public Netportal() throws Exception{								//Construct class Lazismu
		logger.info("Netportal automation has been started !");			//Load konfigurasi library buat log
		PropertyConfigurator.configure("D:\\Java\\log.properties");		//Calling function
		loadProperties();
		startAutomation();
	}
	
	private void loadProperties() {
		try {
			
			File file = new File("setting.properties");
			fis = new FileInputStream(file);
			Properties p = new Properties();
			p.load(fis);
			
			File fileSetting = new File("setting.properties");
			fisSetting = new FileInputStream(fileSetting);
			
			pathLoc = new Properties();
			pathLoc.load(fisSetting);
			path = pathLoc.getProperty("automation.app.config.dir");	//Ambil path partisi komputer, contoh: D: Local Disk, E: Local Disk untuk diset ke object path.
			logger.info("Local Disk: "+path);
			
			if(p.getProperty("automation.app.test.type").equalsIgnoreCase("positif")) {
				File file2 = new File(path+"Java\\netportal-article.properties");
				fis2 = new FileInputStream(file2);
				
				Properties prop = new Properties();
				this.p = prop;
				this.p.load(fis2);
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			logger.error("There is error message: " + e.getMessage());
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("There is error message: " + e.getMessage());
		} finally {
			try {
				fis2.close();
			} catch (IOException e) {
				e.printStackTrace();
				logger.error("There is error message: " + e.getMessage());
			}
		}
		
	}
	
	//Open browser
	public void chromeBrowser() {
		System.setProperty("webdriver.chrome.driver", path+"Java\\ChromeDriver\\chromedriver.exe");
		objDriver = new ChromeDriver();
		objDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		objDriver.get("http://cms.netportal.dev.codigo.id");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("moz:firefoxOptions", true);
	}
	
	//Open browser
	public void firefoxBrowser() {
		System.setProperty("webdriver.gecko.driver", path+"Java\\Gecko\\geckodriver-v0.18.0-win64\\geckodriver.exe");
		FirefoxProfile profile = new FirefoxProfile();
		profile.setPreference("security.insecure_field_warning.contextual.enabled", false);
		profile.setPreference("security.insecure_password.ui.enabled", false);
		objDriver = new FirefoxDriver(profile);
		objDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		objDriver.get("http://cms.netportal.dev.codigo.id");
	}
	
	//Automation test akan dimulai dari sini, karena function ini yang akan memanggil browser pertama kali dan akan melakukan step selanjutnya sekali berurut.
	public void startAutomation() throws Exception {
		logger.info("INFO : Open browser ! >>>>");
		
		File file = new File("setting.properties");
		FileInputStream fis = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fis);
		
		//Cek browser yang dipilih
		if( prop.getProperty("automation.app.browser").equalsIgnoreCase("firefox") ) {
			firefoxBrowser();
		} else if( prop.getProperty("automation.app.browser").equalsIgnoreCase("chrome") ) {
			chromeBrowser();
		} else {
			logger.info("Browser not found !");
			System.exit(0); //Kalo browser yang dipilih tidak sesuai maka aplikasi akan exit.
		}
		
		logger.info("http://cms.netportal.dev.codigo.id has been opened ! >>>> ");
		login();
	}
	
	@SuppressWarnings("static-access")
	private void timeout(int param) {
		try {
			Thread t = new Thread();
			t.sleep(param);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private void enterButton() {
		logger.info("Enter >>>>");
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		
	}
	
	private void tabButton() {
		try {
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_TAB);
			robot.keyRelease(KeyEvent.VK_TAB);
		} catch (AWTException e) {
			e.printStackTrace();
		}
		
	}
	
	//Upload image thumbnail
	private void uploadImage() {
		logger.info("Upload image >>>>");
		
		objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[3]/div/div[3]/div/div/div/span/span[1]")).click();
		
		try {
			Robot robot = new Robot();
			
			//copy image location lalu akan dipastekan di pop up explorer
			String thumbnail = p.getProperty("article.image.location");
			StringSelection stringSel = new StringSelection(thumbnail);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSel, null);
			
			robot.setAutoDelay(1000);
			
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			
			robot.setAutoDelay(1000);
			
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
	
	

	private void checkingMediaSupport() {
		logger.info("Checking media support >>>>");
		
		try {
			Robot robot = new Robot();
			
			String mediaSupport = p.getProperty("article.content.mediaSupportImg");
			StringSelection stringSel = new StringSelection(mediaSupport);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(stringSel, null);
			
			robot.setAutoDelay(1000);
			
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			
			robot.keyRelease(KeyEvent.VK_CONTROL);
			robot.keyRelease(KeyEvent.VK_V);
			
			robot.setAutoDelay(1000);
			
			robot.keyPress(KeyEvent.VK_ENTER);
			robot.keyRelease(KeyEvent.VK_ENTER);
			
		} catch (AWTException e) {
			e.printStackTrace();
		}
		
	}
	
	//add article di cms spotlight netportal
	private void addArticles() {
		
		logger.info("Add articles >>>> ");
		String getChannelType = p.getProperty("article.channel.type");
		WebElement webElement = objDriver.findElement(By.id("channel"));
		Select selectDropdown = new Select(webElement);
		selectDropdown.selectByVisibleText(getChannelType);
		
		String getArticlesType = p.getProperty("article.type");
		
		//Checking article typenya, pilihannya ada standard, gallery, dan article list
		if( getArticlesType.equalsIgnoreCase("standard")) {
			objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[2]/div/div/label[1]")).click();
			String getMainMedia = p.getProperty("article.main.media");
			
			//Checking tipe main media yang akan dipost. Apakah video, image atauu hanya sebuah link tautan.
			if( getMainMedia.equals("video")) {
				objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[3]/div/div[1]/fieldset/label[1]")).click();
				objDriver.findElement(By.id("video")).click();
				
				WebElement captionVideo = objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[3]/div/div[5]/div/div/input"));
				String getCaptionVideo = p.getProperty("article.caption.video");
				captionVideo.sendKeys(getCaptionVideo);
				contentArticleStandard();
				
			} else if( getMainMedia.equals("image")) {
				objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[3]/div/div[1]/fieldset/label[2]")).click();
				uploadImage();
				String getCaptionImage = p.getProperty("article.caption.image");
				WebElement captionImage = objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[3]/div/div[5]/div/div/input"));
				captionImage.clear();
				captionImage.sendKeys(getCaptionImage);
				captionImage.sendKeys(Keys.TAB);
				enterButton();
				timeout(3000);
				checkingMediaSupport();
				contentArticleStandard();
				
			} else if( getMainMedia.equals("link")) {
				objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[3]/div/div[1]/fieldset/label[3]"));
			}
			
		} else if( getArticlesType.equalsIgnoreCase("gallery")) {
			objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[2]/div/div/label[2]")).click();
		} else if( getArticlesType.equalsIgnoreCase("articlelist")) {
			objDriver.findElement(By.xpath("/html/body/section/section/div[2]/form/div[1]/section[2]/div/div/label[3]")).click();
		} else {
			logger.error("There is error message: Missing choice ! ");
		}
	}
	
	//Content article standard
	private void contentArticleStandard() {
		WebElement getTitleEle = objDriver.findElement(By.xpath("//*[@id=\"inputJudul\"]"));
		String title = p.getProperty("article.content.title");
		getTitleEle.clear();
		getTitleEle.sendKeys(title);
		
		WebElement getSubTitleEle = objDriver.findElement(By.xpath("//*[@id=\"inputSubjudul\"]"));
		String subTitle = p.getProperty("article.content.subtitle");
		getSubTitleEle.clear();
		getSubTitleEle.sendKeys(subTitle);
		
		WebElement getDescEle = objDriver.findElement(By.xpath("//*[@id=\"inputDesc\"]"));
		String desc = p.getProperty("article.content.desc");
		getDescEle.clear();
		getDescEle.sendKeys(desc);
		getDescEle.sendKeys(Keys.TAB);
		
		WebElement getActiveEle = objDriver.switchTo().activeElement();
		getActiveEle.sendKeys(Keys.ENTER);
		String getLocation = p.getProperty("article.content.location");
		logger.info("Ini keyword: " + getLocation);
		WebElement getLocEle = objDriver.findElement(By.id("select2-lokasi-results"));
		
		List<WebElement> listLocations = getLocEle.findElements(By.className("select2-results__option"));
		
		timeout(2000);
		
		for( WebElement e: listLocations) {
			if( e.getText().equals(getLocation)) {
				e.click();
				break;
			}
		}
		
		WebElement getMetaTitleEle = objDriver.findElement(By.xpath("//*[@id=\"metaTitle\"]"));
		String getMetaTitle = p.getProperty("article.content.meta.title");
		getMetaTitleEle.clear();
		getMetaTitleEle.sendKeys(getMetaTitle);
		
		WebElement getMetaDescEle = objDriver.findElement(By.xpath("//*[@id=\"metaDesc\"]"));
		String getMetaDesc = p.getProperty("article.content.meta.desc");
		getMetaDescEle.clear();
		getMetaDescEle.sendKeys(getMetaDesc);
		getMetaDescEle.sendKeys(Keys.TAB);
		
		WebElement ele = objDriver.switchTo().activeElement();
		ele.clear();
		ele.sendKeys(Keys.DOWN);
		
		String getKeyword = p.getProperty("article.content.keyword");
		WebElement autoComplete = objDriver.findElement(By.id("select2-keywords-results"));
		
		List<WebElement> listAutoComplete = autoComplete.findElements(By.className("select2-results__option"));
		
		timeout(3000);
		
		for( WebElement e: listAutoComplete) {
			if( e.getText().equals(getKeyword)) {
				enterButton();
				break;
			}
		}
		
		tabButton();
		timeout(3000);
		
		WebElement activeEle = objDriver.switchTo().activeElement();
		String getPeopleRelated = p.getProperty("article.content.peoplerelated");
		activeEle.sendKeys(Keys.DOWN);
		
		WebElement peopleRelated = objDriver.findElement(By.id("select2-database-results"));
		List<WebElement> listPeopleRelated = peopleRelated.findElements(By.className("select2-results__option"));
		
		for( WebElement e: listPeopleRelated) {
			if( e.getText().contains(getPeopleRelated)) {
				e.click();
				break;
			}
		}
		
		tabButton();
		
		WebElement getContentEditor = objDriver.switchTo().activeElement();
		String getContentValue = p.getProperty("article.content.value");
		getContentEditor.sendKeys(getContentValue);
		logger.info("Isi content adalah " + getContentValue);
		
		WebElement publish = objDriver.findElement(By.id("toPublishs"));
		JavascriptExecutor js = (JavascriptExecutor) objDriver;
		js.executeScript("arguments[0].click();", publish);
		
	}

	public void makeArticles() {
		objDriver.findElement(By.linkText("Content Management")).click(); //content management
		WebElement ele = objDriver.findElement(By.linkText("Articles")); //articles
		ele.click();
		timeout(5000);
		objDriver.findElement(By.xpath("/html/body/aside[2]/div/ul/li[2]/a/span[text()='All Channel']")).click(); //all channel
		objDriver.findElement(By.xpath("/html/body/section/section/div[2]/div/section/div/div/section/div/a")).click(); //add article
		addArticles();
	}
	
	public void login() {
		logger.info("Login into Netportal CMS >>>> ");
		String username = p.getProperty("login.username");
		String password = p.getProperty("login.password");
		objDriver.findElement(By.id("username")).sendKeys(username);
		objDriver.findElement(By.id("password")).sendKeys(password);
		
		timeout(5000);
		objDriver.findElement(By.id("login")).click();
		
		int size = objDriver.findElements(By.xpath("/html/body/div[1]/form/div[1]/span")).size();
		
		if( size == 0) {
			logger.info("Validasi berhasil >>>>");
			String getSuccessNotif = objDriver.findElement(By.xpath("//div[@id='toast-container']/div/div")).getText();
			logger.info("Get success notif: " + getSuccessNotif);
			if( getSuccessNotif.equalsIgnoreCase("Selamat Datang")) {
				String successNotif = "Login Sucessfully !";
				logger.info("Login Successfully !");
				new SendToExcelNetportal(dateFormat.format(date), successNotif, objDriver.getCurrentUrl());
//				makeArticles();
			}
		} else {
			String getError = objDriver.findElement(By.xpath("/html/body/div[1]/form/div[1]/span")).getText();
			logger.info("Notif error: " + getError);
			new SendToExcelNetportal(dateFormat.format(date), getError, objDriver.getCurrentUrl());
			System.exit(0);
		}
	}

}
