package com.selenium.app.spotlight;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class Spotlight {
	
	private WebDriver objDriver;
	private Logger logger = Logger.getLogger("Lazismu");
	private Properties p;
	private String path;
	private FileInputStream fis;
	private DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
	private Date date = new Date();
	private String baseUrl = "http://cms.spotlight.cips.stg.codigo.id/";
	
	public Spotlight(Properties prop) throws Exception {
		logger.info("Lazismu automation has been started !");
		this.p = prop;
		
		this.path =  prop.getProperty("automation.app.config.dir");
		PropertyConfigurator.configure(path+"Java\\log.properties");		//Load konfigurasi library buat log
		loadProperties();												//Calling function
		startAutomation();
	}
	
	private void robot(String event) {
		try {
			
			Robot robot = new Robot();
			
			switch (event) {
			case "tab":
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);
				break;
			case "enter":
				robot.keyPress(KeyEvent.VK_ENTER);
				robot.keyRelease(KeyEvent.VK_ENTER);
				break;
			default:
				break;
			}
		} catch (AWTException e) {
			e.printStackTrace();
		}
	}
	
	public Boolean CheckIfElementIsVisible(String by, String element) {
		
		if (by.equalsIgnoreCase("xpath")) {
			Boolean isVisible = objDriver.findElement(By.xpath(element)).isDisplayed();
		    if (isVisible) {
				return true;
			} else {
				return false;
			}
		} else if(by.equalsIgnoreCase("name")) {
			Boolean isVisible = objDriver.findElement(By.name(element)).isDisplayed();
		    if (isVisible) {
				return true;
			} else {
				return false;
			}
		} else if(by.equalsIgnoreCase("id")) {
			Boolean isVisible = objDriver.findElement(By.id(element)).isDisplayed();
		    if (isVisible) {
				return true;
			} else {
				return false;
			}
		} else if(by.equalsIgnoreCase("class")) {
			Boolean isVisible = objDriver.findElement(By.className(element)).isDisplayed();
		    if (isVisible) {
				return true;
			} else {
				return false;
			}
		} else if(by.equalsIgnoreCase("tag")) {
			Boolean isVisible = objDriver.findElement(By.tagName(element)).isDisplayed();
		    if (isVisible) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
		
	}

	private void loadProperties() {
		try {
			
			File file = new File(this.path+"Java\\spotlight-article.properties");
			fis = new FileInputStream(file);
			
			Properties prop = new Properties();
			this.p = prop;
			this.p.load(fis);
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	@SuppressWarnings("static-access")
	private void timeout(int param) {
		try {
			Thread t = new Thread();
			t.sleep(param);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	public void chromeBrowser() {
		logger.info("Chrome browser ! >>>>");
		System.setProperty("webdriver.chrome.driver", path + "Java\\ChromeDriver\\chromedriver.exe");
		objDriver = new ChromeDriver();
		objDriver.get("http://cms.spotlight.cips.stg.codigo.id/login");
		DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability("moz:firefoxOptions", true);
	}
	
	public void firefoxBrowser() {
		logger.info("Firefox browser ! >>>>");
		System.setProperty("webdriver.gecko.driver", path + "Java\\Gecko\\geckodriver-v0.18.0-win64\\geckodriver.exe");
		objDriver = new FirefoxDriver();
		objDriver.get("http://cms.spotlight.cips.stg.codigo.id/login");
	}

	
	public void startAutomation() throws Exception {
		
		logger.info("Open browser ! >>>>");
		File file = new File(path+"\\Java\\setting.properties");
		FileInputStream fis = new FileInputStream(file);
		Properties prop = new Properties();
		prop.load(fis);
		
		if( prop.getProperty("automation.app.browser").equalsIgnoreCase("firefox") ) {
			firefoxBrowser();
		} else if( prop.getProperty("automation.app.browser").equalsIgnoreCase("chrome") ) {
			chromeBrowser();
		} else {
			logger.info("Browser not found !");
			System.exit(0);
		}
		
		logger.info("http://cms.spotlight.cips.dev.codigo.id/login has been opened ! >>>> ");
		login();
		createArticle();
	}

	private void createArticle() {
		
		logger.info("Create Article >>>>");
		timeout(5000);
		objDriver.findElement(By.linkText("Content")).click();
		objDriver.findElement(By.linkText("Article")).click();
	    objDriver.findElement(By.linkText("Add Article")).click();
	    
	    String type = p.getProperty("article.type");
	    if (type.equalsIgnoreCase("standard")) {
			standard();
		} else if(type.equalsIgnoreCase("gallery")) {
			gallery();
		} else if(type.equalsIgnoreCase("group")){
			group();
		} else {
			logger.error("Article type was not found !");
		}
	}
	
	private void standard() {
			
		if (p.getProperty("subchannel").equalsIgnoreCase("")) {
			objDriver.findElement(By.xpath("//html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div[1]/div/div[1]/div[3]/input")).clear();
		    objDriver.findElement(By.xpath("//html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div[1]/div/div[1]/div[3]/input"))
		    .sendKeys("C:\\Users\\Dimas Rifqi\\Pictures\\meetup.png");
		    
		    objDriver.findElement(By.id("btnThumbnailUpload")).clear();
		    objDriver.findElement(By.id("btnThumbnailUpload")).sendKeys(p.getProperty("thumbnail"));
		    
		    objDriver.findElement(By.id("captionmainmedia")).sendKeys(p.getProperty("caption"));
		    
		    objDriver.findElement(By.name("content_title")).clear();
		    objDriver.findElement(By.name("content_title")).sendKeys(p.getProperty("title"));
		    objDriver.findElement(By.name("content_description")).clear();
		    objDriver.findElement(By.name("content_description")).sendKeys(p.getProperty("description"));
		    objDriver.findElement(By.id("geocomplete")).clear();
		    objDriver.findElement(By.id("geocomplete")).sendKeys(p.getProperty("location"));
		    objDriver.findElement(By.name("content_subtitle")).sendKeys(p.getProperty("subtitle"));
		    objDriver.findElement(By.name("content_subtitle")).sendKeys(Keys.TAB);
		    
		    WebElement contentEle = objDriver.switchTo().activeElement();
		    contentEle.clear();
		    contentEle.sendKeys(p.getProperty("content"));
		    
		    objDriver.findElement(By.name("seo_meta_title")).clear();
		    objDriver.findElement(By.name("seo_meta_title")).sendKeys(p.getProperty("meta.title"));
		    objDriver.findElement(By.name("seo_meta_description")).clear();
		    objDriver.findElement(By.name("seo_meta_description")).sendKeys(p.getProperty("meta.desc"));

		    //Start select reporter dan editor
		    WebElement reporterEle = objDriver.findElement(By.id("reporterSelectBoxItText"));
		    reporterEle.click();
		    WebElement reporterList = objDriver.findElement(By.id("reporterSelectBoxItOptions"));
		    List<WebElement> reporterName = reporterList.findElements(By.tagName("li"));
		    if (reporterName.size() > 0) {
		    	for(WebElement name: reporterName) {
		    		if(!p.getProperty("reporter").equals("")) {
		    			if (name.getText().equals(p.getProperty("reporter"))) {
				    		name.click();
						}
		    		} else {
		    			logger.error("Reporter can't be null !");
		    			System.exit(0);
		    		} 
			    }
			}
		    
		    WebElement writerEle = objDriver.findElement(By.id("reporterSelectBoxItText"));
		    writerEle.click();
		    WebElement writerList = objDriver.findElement(By.id("reporterSelectBoxItOptions"));
		    List<WebElement> writerName = writerList.findElements(By.tagName("li"));
		    if (writerName.size() > 0) {
		    	for(WebElement name: writerName) {
		    		if(!p.getProperty("writer").equals("")) {
		    			if (name.getText().equals(p.getProperty("writer"))) {
				    		name.click();
						}
		    		} else {
		    			logger.error("Writer can't be null !");
		    			System.exit(0);
		    		} 
			    }
			}
		    
		    WebElement editorEle = objDriver.findElement(By.id("editorSelectBoxItText"));
		    editorEle.click();
		    WebElement editorList = objDriver.findElement(By.id("reporterSelectBoxItOptions"));
		    List<WebElement> editorName = editorList.findElements(By.tagName("li"));
		    if (editorName.size() > 0) {
		    	for(WebElement name: editorName) {
		    		if(!p.getProperty("editor").equals("")) {
		    			if (name.getText().equals(p.getProperty("editor"))) {
				    		name.click();
						}
		    		} else {
		    			logger.error("Editor can't be null !");
		    			System.exit(0);
		    		} 
			    }
			}
		    
		    //END
		    
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input")).clear();
		    
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
		    .sendKeys(p.getProperty("keyword"));
		    timeout(1000);
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
		    .sendKeys(Keys.ENTER);
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
		    .sendKeys(Keys.TAB);
		    
		    WebElement peopleEle = objDriver.switchTo().activeElement();
		    peopleEle.sendKeys(p.getProperty("people"));
		    timeout(1000);
		    peopleEle.sendKeys(Keys.ENTER);
		    peopleEle.sendKeys(Keys.TAB);
		    
		    objDriver.findElement(By.cssSelector("a[class='save spot-button btn--primary btn--large u-mt--16 u-fz--14']")).click();
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/form/div[2]/div[6]/div[2]/a")).click();
		    
		    List<WebElement> getError = objDriver.findElements(By.className("ada-error"));
		    List<String> errorEle = new ArrayList<String>(); 
		    if (getError.size() !=0) {
				for(WebElement element: getError) {
					errorEle.add(element.getText());
				}
			}
		    
		} else {
			
		}
	}

	private void gallery() {
		
		WebElement typeEle = objDriver.findElement(By.id("selectionChannelSelectBoxIt"));
		typeEle.click();
	    WebElement typeList = objDriver.findElement(By.id("selectionChannelSelectBoxItOptions"));
	    List<WebElement> typeName = typeList.findElements(By.tagName("li"));
	    if (typeName.size() > 0) {
	    	for(WebElement name: typeName) {
    			if (name.getText().equalsIgnoreCase(p.getProperty("article.type"))) {
		    		name.click();
				}
		    }
		}
	    
	    WebElement channelEle = objDriver.findElement(By.id("selectionChannel1SelectBoxItText"));
	    channelEle.click();
	    WebElement channelList = objDriver.findElement(By.id("selectionChannel1SelectBoxItOptions"));
	    List<WebElement> channelName = channelList.findElements(By.tagName("li"));
	    if (typeName.size() > 0) {
	    	for(WebElement name: channelName) {
    			if (name.getText().equalsIgnoreCase(p.getProperty("channel"))) {
		    		name.click();
				}
		    }
		}
	    
	    objDriver.findElement(By.id("btnThumbnailUpload")).clear();
	    objDriver.findElement(By.id("btnThumbnailUpload")).sendKeys(p.getProperty("thumbnail"));
	    objDriver.findElement(By.name("content_title")).clear();
	    objDriver.findElement(By.name("content_title")).sendKeys(p.getProperty("title"));
	    objDriver.findElement(By.name("content_description")).clear();
	    objDriver.findElement(By.name("content_description")).sendKeys(p.getProperty("description"));
	    objDriver.findElement(By.id("geocomplete")).clear();
	    objDriver.findElement(By.id("geocomplete")).sendKeys(p.getProperty("location"));
	    objDriver.findElement(By.name("content_subtitle")).sendKeys(p.getProperty("subtitle"));
	    objDriver.findElement(By.name("content_subtitle")).sendKeys(Keys.TAB);
	    
	    WebElement contentEle = objDriver.switchTo().activeElement();
	    contentEle.clear();
	    contentEle.sendKeys(p.getProperty("content"));
	    
	    objDriver.findElement(By.id("formsubartimage")).clear();
	    objDriver.findElement(By.id("formsubartimage")).sendKeys(p.getProperty("gallery.image"));
	    timeout(2000);
	    objDriver.findElement(By.id("inputcaption")).clear();
	    objDriver.findElement(By.id("inputcaption")).sendKeys(p.getProperty("gallery.caption"));
	    objDriver.findElement(By.id("addNewGallery")).click();
	    
	    objDriver.findElement(By.name("seo_meta_title")).clear();
	    objDriver.findElement(By.name("seo_meta_title")).sendKeys(p.getProperty("meta.title"));
	    objDriver.findElement(By.name("seo_meta_description")).clear();
	    objDriver.findElement(By.name("seo_meta_description")).sendKeys(p.getProperty("meta.desc"));

	    //Start select reporter dan editor
	    WebElement reporterEle = objDriver.findElement(By.id("reporterSelectBoxItText"));
	    reporterEle.click();
	    WebElement reporterList = objDriver.findElement(By.id("reporterSelectBoxItOptions"));
	    List<WebElement> reporterName = reporterList.findElements(By.tagName("li"));
	    if (reporterName.size() > 0) {
	    	for(WebElement name: reporterName) {
	    		if(!p.getProperty("reporter").equals("")) {
	    			if (name.getText().equalsIgnoreCase(p.getProperty("reporter"))) {
			    		name.click();
					}
	    		} else {
	    			logger.error("Reporter can't be null !");
	    			System.exit(0);
	    		}
		    }
		}
	    
	    robot("tab");
	    
	    WebElement writerEle = objDriver.switchTo().activeElement();
	    writerEle.click();
	    
	    WebElement writerList = objDriver.findElement(By.id("writerSelectBoxItOptions"));
	    List<WebElement> writerName = writerList.findElements(By.tagName("li"));
	    if (writerName.size() > 0) {
	    	for(WebElement name: writerName) {
	    		if(!p.getProperty("writer").equals("")) {
	    			if (name.getText().equalsIgnoreCase(p.getProperty("writer"))) {
			    		name.click();
					}
	    		} else {
	    			logger.error("Writer can't be null !");
	    			System.exit(0);
	    		}
		    }
		}
	    
	    WebElement editorEle = objDriver.findElement(By.id("editorSelectBoxItText"));
	    editorEle.click();
	    WebElement editorList = objDriver.findElement(By.id("editorSelectBoxItOptions"));
	    List<WebElement> editorName = editorList.findElements(By.tagName("li"));
	    if (editorName.size() > 0) {
	    	for(WebElement name: editorName) {
	    		if(!p.getProperty("editor").equals("")) {
	    			if (name.getText().equalsIgnoreCase(p.getProperty("editor"))) {
			    		name.click();
					}
	    		} else {
	    			logger.error("Editor can't be null !");
	    			System.exit(0);
	    		}
		    }
		}
	    //END
	    
	    //START KEYWORD DAN PEOPLE RELATED
	    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input")).clear();
	    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
	    .sendKeys(p.getProperty("keyword"));
	    timeout(1000);
	    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
	    .sendKeys(Keys.ENTER);
	    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
	    .sendKeys(Keys.TAB);
	    
	    WebElement peopleEle = objDriver.switchTo().activeElement();
	    peopleEle.sendKeys(p.getProperty("people.related"));
	    timeout(1000);
	    peopleEle.sendKeys(Keys.ENTER);
	    peopleEle.sendKeys(Keys.TAB);
	    //END
	    
	    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/form/div[2]/div[6]/div[2]/a")).click();
	    
	    List<WebElement> getError = objDriver.findElements(By.className("ada-error"));
	    List<String> errorEle = new ArrayList<String>(); 
	    if (getError.size() !=0) {
			for(WebElement element: getError) {
				errorEle.add(element.getText());
			}
		}
		
	}

	private void group() {
	    
	    if (p.getProperty("subchannel").equalsIgnoreCase("")) {
	    	
	    	WebElement typeEle = objDriver.findElement(By.id("selectionChannelSelectBoxItContainer"));
			typeEle.click();
		    WebElement typeList = objDriver.findElement(By.id("selectionChannelSelectBoxIt"));
		    List<WebElement> typeName = typeList.findElements(By.tagName("li"));
		    if (typeName.size() > 0) {
		    	for(WebElement name: typeName) {
	    			if (name.getText().equals(p.getProperty("article.type"))) {
			    		name.click();
					}
			    }
			}
	    	
			objDriver.findElement(By.xpath("//html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div[1]/div/div[1]/div[3]/input")).clear();
		    objDriver.findElement(By.xpath("//html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[2]/div[2]/div/div[2]/div/div[1]/div/div[1]/div/div[1]/div[3]/input"))
		    .sendKeys("C:\\Users\\Dimas Rifqi\\Pictures\\meetup.png");
		    
		    objDriver.findElement(By.id("btnThumbnailUpload")).clear();
		    objDriver.findElement(By.id("btnThumbnailUpload")).sendKeys(p.getProperty("thumbnail"));
		    
		    objDriver.findElement(By.id("captionmainmedia")).sendKeys(p.getProperty("caption"));
		    
		    objDriver.findElement(By.name("content_title")).clear();
		    objDriver.findElement(By.name("content_title")).sendKeys(p.getProperty("title"));
		    objDriver.findElement(By.name("content_description")).clear();
		    objDriver.findElement(By.name("content_description")).sendKeys(p.getProperty("description"));
		    objDriver.findElement(By.id("geocomplete")).clear();
		    objDriver.findElement(By.id("geocomplete")).sendKeys(p.getProperty("location"));
		    objDriver.findElement(By.name("content_subtitle")).sendKeys(p.getProperty("subtitle"));
		    objDriver.findElement(By.name("content_subtitle")).sendKeys(Keys.TAB);
		    
		    WebElement contentEle = objDriver.switchTo().activeElement();
		    contentEle.clear();
		    contentEle.sendKeys(p.getProperty("content"));
		    
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[6]/div[2]/div/div[1]/a")).click();
		    timeout(2000);
		    objDriver.findElement(By.name("SubArtTitle")).clear();
		    objDriver.findElement(By.name("SubArtTitle")).sendKeys(p.getProperty("group.subarticle.title"));
		    objDriver.findElement(By.name("SubArtTitle")).sendKeys(Keys.TAB);
		    
		    WebElement subArticle = objDriver.switchTo().activeElement();
		    subArticle.clear();
		    subArticle.sendKeys(p.getProperty("group.subarticle.content"));
		    
		    objDriver.findElement(By.name("seo_meta_title")).clear();
		    objDriver.findElement(By.name("seo_meta_title")).sendKeys(p.getProperty("meta.title"));
		    objDriver.findElement(By.name("seo_meta_description")).clear();
		    objDriver.findElement(By.name("seo_meta_description")).sendKeys(p.getProperty("meta.desc"));

		    //Start select reporter dan editor
		    WebElement reporterEle = objDriver.findElement(By.id("reporterSelectBoxItText"));
		    reporterEle.click();
		    WebElement reporterList = objDriver.findElement(By.id("reporterSelectBoxItOptions"));
		    List<WebElement> reporterName = reporterList.findElements(By.tagName("li"));
		    if (reporterName.size() > 0) {
		    	for(WebElement name: reporterName) {
		    		if(!p.getProperty("reporter").equals("")) {
		    			if (name.getText().equals(p.getProperty("reporter"))) {
				    		name.click();
						}
		    		} else {
		    			logger.error("Reporter can't be null !");
		    			System.exit(0);
		    		} 
			    }
			}
		    
		    WebElement writerEle = objDriver.findElement(By.id("reporterSelectBoxItText"));
		    writerEle.click();
		    WebElement writerList = objDriver.findElement(By.id("reporterSelectBoxItOptions"));
		    List<WebElement> writerName = writerList.findElements(By.tagName("li"));
		    if (writerName.size() > 0) {
		    	for(WebElement name: writerName) {
		    		if(!p.getProperty("writer").equals("")) {
		    			if (name.getText().equals(p.getProperty("writer"))) {
				    		name.click();
						}
		    		} else {
		    			logger.error("Writer can't be null !");
		    			System.exit(0);
		    		} 
			    }
			}
		    
		    WebElement editorEle = objDriver.findElement(By.id("editorSelectBoxItText"));
		    editorEle.click();
		    WebElement editorList = objDriver.findElement(By.id("reporterSelectBoxItOptions"));
		    List<WebElement> editorName = editorList.findElements(By.tagName("li"));
		    if (editorName.size() > 0) {
		    	for(WebElement name: editorName) {
		    		if(!p.getProperty("editor").equals("")) {
		    			if (name.getText().equals(p.getProperty("editor"))) {
				    		name.click();
						}
		    		} else {
		    			logger.error("Editor can't be null !");
		    			System.exit(0);
		    		} 
			    }
			}
		    
		    //END
		    
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input")).clear();
		    
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
		    .sendKeys(p.getProperty("keyword"));
		    timeout(1000);
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
		    .sendKeys(Keys.ENTER);
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[1]/div/div[7]/div[2]/div/form/div/div[1]/label/span/span[1]/span/ul/li/input"))
		    .sendKeys(Keys.TAB);
		    
		    WebElement peopleEle = objDriver.switchTo().activeElement();
		    peopleEle.sendKeys(p.getProperty("people"));
		    timeout(1000);
		    peopleEle.sendKeys(Keys.ENTER);
		    peopleEle.sendKeys(Keys.TAB);
		    
		    objDriver.findElement(By.cssSelector("a[class='save spot-button btn--primary btn--large u-mt--16 u-fz--14']")).click();
		    objDriver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[1]/div[2]/div/div/div[2]/div/form/div[2]/div[6]/div[2]/a")).click();
		    
		    List<WebElement> getError = objDriver.findElements(By.className("ada-error"));
		    List<String> errorEle = new ArrayList<String>(); 
		    if (getError.size() !=0) {
				for(WebElement element: getError) {
					errorEle.add(element.getText());
				}
			}
		    
		} else {
			
		}
	}

	private void login() {
		
		logger.info("Login into Spotlight >>>>");
		String username = p.getProperty("login.username");
		String password = p.getProperty("login.password");
		
		objDriver.findElement(By.id("email")).sendKeys(username);
		objDriver.findElement(By.id("passwordInput")).sendKeys(password);
		objDriver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div/div/div/div/div[1]/div/div/form/div[3]/button")).click();
		
	}

}
